import React from 'react';
import './App.css';



const PlanetsList = () => {
  const titleElement = React.createElement('h1', {
    style: { color: '#999', fontSize: '19px' },
  }, 'Solar system planets');

  return (
    <div>
      {titleElement}
      <ul className="planets-list">
        <li>Mercury</li>
        <li>Venus</li>
        <li>Earth</li>
        <li>Mars</li>
        <li>Jupiter</li>
        <li>Saturn</li>
        <li>Uranus</li>
        <li>Neptune</li>
      </ul>
    </div>
  );
};





function App() {
  return (
    <div className="App">
      <PlanetsList />
    </div>
  );
}

export default App;